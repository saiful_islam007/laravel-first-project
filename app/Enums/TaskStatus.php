<?php

namespace App\Enums;



//use Illuminate\Validation\Rules\Enum;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class TaskStatus extends Enum
{
    //const key = value
    const Pending =   0;    //here Pending is key and 0 is value
    const Ongoing =   1;
    const Done = 2;

    public static function getDescription($value): string
    {
        if ($value === self::Ongoing) {
            return 'On-going';
        }

        return parent::getDescription($value);
    }
}
