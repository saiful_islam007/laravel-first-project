@extends('layouts.app')

@section('contents')
    <a href="{{ route('categories.create') }}" class="btn btn-success">Add New Category</a>
    <hr> 
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Name</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($category_list as $item)
        <tr>
            <td>{{ $item->name }}</td>
            <td>
                <a href="{{ url("/categories/$item->id/edit") }}" class="btn btn-danger btn-sm">Update</a>
                {{-- <a href="" class="btn btn-danger btn-sm">Delete</a> --}}

                <form action="{{ url("/categories/$item->id") }}" method="POST" onsubmit="return confirm('Do you really want to delete this category?');">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
          </tr>
        @endforeach 
    </tbody>
  </table>
@endsection 


