@extends('layouts.app')

@section('contents')
   <h3>Update Task</h3>
   <hr>
   <form class="form-horizontal" action="{{ url("/tasks/$tasks->id") }}" method="POST">
    @method("put")
    @csrf
    <div class="form-group">
      <label class="control-label col-sm-2">Task Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $tasks->name }}" placeholder="Enter a task Name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Category:</label>
      <div class="col-sm-10">
          <select class="form-control" name="category_id">
            <option value="">--Select a Category--</option>
              @foreach ($category_list as $item)
                  <option value="{{ $item->id }}" {{ $tasks->category_id==$item->id ? 'selected':''}}>{{ $item->name }}</option>
              @endforeach
            </select> 
      </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2">Task Details:</label>
        <div class="col-sm-10">
          <textarea name="details"  cols="30" rows="10" class="form-control">{{ old('details') }}</textarea>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Task Deadline:</label>
        <div class="col-sm-10">
          <input type="date" name="deadline" value="{{ $tasks->deadline }}" class="form-control">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Status:</label>
        <div class="col-sm-10">
            <select class="form-control" name="status">
              <option value="">--Select a Status--</option>
                @foreach ($task_status as $x => $status)
                    <option value="{{ $x }}" {{ $tasks->status ==$x ? 'selected':''}}>{{ $status }}</option>
                @endforeach
              </select>
        </div>
      </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Update</button>
      </div>
    </div>
  </form>
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection 