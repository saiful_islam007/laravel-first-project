<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TaskController;
use App\Models\Task;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/categories',[CategoryController::class,'index'])->middleware('auth')->name('categories.index');
Route::get('/categories/create',[CategoryController::class,'create'])->middleware('auth')->name('categories.create');
Route::post('/categories',[CategoryController::class,'store'])->middleware('auth')->name('categories.store');
Route::get('/categories/{id}/edit',[CategoryController::class,'edit'])->middleware('auth')->name('categories.edit');
Route::put('/categories/{id}',[CategoryController::class,'update'])->middleware('auth')->name('categories.update');
Route::delete('/categories/{id}',[CategoryController::class,'destroy'])->middleware('auth')->name('categories.delete');


//for task
Route::get('/tasks',[TaskController::class,'index'])->middleware('auth')->name('tasks.index');
Route::get('/tasks/create',[TaskController::class,'create'])->middleware('auth')->name('tasks.create');
Route::post('/tasks',[TaskController::class,'store'])->middleware('auth')->name('tasks.store');
Route::get('/tasks/{id}/edit',[TaskController::class,'edit'])->middleware('auth')->name('tasks.edit');
Route::put('/tasks/{id}',[TaskController::class,'update'])->middleware('auth')->name('tasks.update');
Route::delete('/tasks/{id}',[TaskController::class,'destroy'])->middleware('auth')->name('tasks.delete');


require __DIR__.'/auth.php';
 